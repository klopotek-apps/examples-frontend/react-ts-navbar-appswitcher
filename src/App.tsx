import React from 'react';
import Navbar from './components/Navbar';

function App() {
  return (
    <div>
      <header >
      <Navbar/>
      </header>
      <main>
        <div>
          Main
        </div>
      </main>
    </div>
  );
}

export default App;
